# Running API & Tests 
Download the files and unzip in your preferred directory. From the root directory, the API can be run with `go run main.go` and will be live on port `8080`. To run the tests, use `go test`. 

# Verifying Correctness
You can import the provided `tritura_assessment.postman_collection.json` file into Postman to run your tests, as this is how I verified the API was functioning correctly. You can also use [Curl](https://curl.se/docs/manpage.html), but I would recommend using Postman as it is a bit simpler to implement.