package main

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
)

// Clear out the tasks variable as multiple tests use it, then create the router
func setupTestRouter() *gin.Engine {
	tasks = []Task{}
	return setupRouter()
}

func TestGetTasks(t *testing.T) {
	r := setupTestRouter()

	req, _ := http.NewRequest("GET", "/tasks", nil)
	w := httptest.NewRecorder()
	r.ServeHTTP(w, req)

	assert.Equal(t, http.StatusOK, w.Code)
}

// TODO Add test for getting all data with tasks already loaded

func TestGetSingleTask(t *testing.T) {
	r := setupTestRouter()

	tasks = []Task{{ID: 1, Title: "Test Task", Description: "Test Description", Status: "Pending"}}

	req, _ := http.NewRequest("GET", "/tasks/1", nil)
	w := httptest.NewRecorder()
	r.ServeHTTP(w, req)

	assert.Equal(t, http.StatusOK, w.Code)
}

func TestGetSingleTaskInvalidData(t *testing.T) {
	r := setupTestRouter()

	req, _ := http.NewRequest("GET", "/tasks/abc", nil)
	w := httptest.NewRecorder()
	r.ServeHTTP(w, req)

	assert.Equal(t, http.StatusBadRequest, w.Code)
}

func TestGetSingleTaskNotFound(t *testing.T) {
	r := setupTestRouter()

	req, _ := http.NewRequest("GET", "/tasks/999", nil)
	w := httptest.NewRecorder()
	r.ServeHTTP(w, req)

	assert.Equal(t, http.StatusNotFound, w.Code)
}

func TestAddTask(t *testing.T) {
	r := setupTestRouter()

	task := Task{
		Title: "Test Task", Description: "Test Description",
	}
	jsonTask, _ := json.Marshal(task)

	req, _ := http.NewRequest("POST", "/tasks", bytes.NewBuffer(jsonTask))
	req.Header.Set("Content-Type", "application/json")
	w := httptest.NewRecorder()
	r.ServeHTTP(w, req)

	assert.Equal(t, http.StatusOK, w.Code)

	var responseTask Task
	err := json.Unmarshal(w.Body.Bytes(), &responseTask)

	assert.NoError(t, err)
	assert.Equal(t, task.Title, responseTask.Title)
	assert.Equal(t, task.Description, responseTask.Description)
}

func TestAddTaskInvalidData(t *testing.T) {
	r := setupTestRouter()

	task := Task{}
	jsonValue, _ := json.Marshal(task)

	req, _ := http.NewRequest("POST", "/tasks", bytes.NewBuffer(jsonValue))
	req.Header.Set("Content-Type", "application/json")
	w := httptest.NewRecorder()
	r.ServeHTTP(w, req)

	assert.Equal(t, http.StatusBadRequest, w.Code)
}

func TestUpdateTask(t *testing.T) {
	r := setupTestRouter()

	tasks = []Task{
		{ID: 1, Title: "Test Task", Description: "Test Description", Status: "Pending"},
	}

	task := Task{
		ID: 1, Title: "Updated Task", Description: "Updated Description", Status: "Completed",
	}
	jsonValue, _ := json.Marshal(task)

	req, _ := http.NewRequest("PUT", "/tasks/1", bytes.NewBuffer(jsonValue))
	req.Header.Set("Content-Type", "application/json")
	w := httptest.NewRecorder()
	r.ServeHTTP(w, req)

	assert.Equal(t, http.StatusOK, w.Code)

	var responseTask Task
	err := json.Unmarshal(w.Body.Bytes(), &responseTask)

	assert.NoError(t, err)
	assert.Equal(t, task.Title, responseTask.Title)
	assert.Equal(t, task.Description, responseTask.Description)
	assert.Equal(t, task.Status, responseTask.Status)
}

func TestUpdateTaskInvalidData(t *testing.T) {
	r := setupTestRouter()

	tasks = []Task{
		{ID: 1, Title: "Test Task", Description: "Test Description", Status: "Pending"},
	}

	task := Task{
		ID: 1, Title: "Updated Task", Description: "Updated Description", Status: "Invalid Status",
	}
	jsonValue, _ := json.Marshal(task)

	req, _ := http.NewRequest("PUT", "/tasks/1", bytes.NewBuffer(jsonValue))
	req.Header.Set("Content-Type", "application/json")
	w := httptest.NewRecorder()
	r.ServeHTTP(w, req)

	assert.Equal(t, http.StatusBadRequest, w.Code)
}
func TestUpdateTaskNotFound(t *testing.T) {
	r := setupTestRouter()

	task := Task{
		ID: 1, Title: "Updated Task", Description: "Updated Description", Status: "In Progress",
	}
	jsonValue, _ := json.Marshal(task)

	req, _ := http.NewRequest("PUT", "/tasks/1", bytes.NewBuffer(jsonValue))
	req.Header.Set("Content-Type", "application/json")
	w := httptest.NewRecorder()
	r.ServeHTTP(w, req)

	assert.Equal(t, http.StatusNotFound, w.Code)
}

func TestUpdateTaskDifferentID(t *testing.T) {
	r := setupTestRouter()

	tasks = []Task{
		{ID: 1, Title: "Test Task", Description: "Test Description", Status: "Pending"},
	}

	task := Task{
		ID: 999, Title: "Updated Task", Description: "Updated Description", Status: "In Progress",
	}
	jsonValue, _ := json.Marshal(task)

	req, _ := http.NewRequest("PUT", "/tasks/1", bytes.NewBuffer(jsonValue))
	req.Header.Set("Content-Type", "application/json")
	w := httptest.NewRecorder()
	r.ServeHTTP(w, req)

	assert.Equal(t, http.StatusBadRequest, w.Code)
}

func TestDeleteTask(t *testing.T) {
	r := setupTestRouter()

	tasks = []Task{
		{ID: 1, Title: "Test Task", Description: "Test Description", Status: "Pending"},
	}

	req, _ := http.NewRequest("DELETE", "/tasks/1", nil)
	w := httptest.NewRecorder()
	r.ServeHTTP(w, req)

	assert.Equal(t, http.StatusNoContent, w.Code)

	req, _ = http.NewRequest("GET", "/tasks/1", nil)
	w = httptest.NewRecorder()
	r.ServeHTTP(w, req)

	assert.Equal(t, http.StatusNotFound, w.Code)
}

func TestDeleteTaskInvalidData(t *testing.T) {
	r := setupTestRouter()

	req, _ := http.NewRequest("DELETE", "/tasks/abc", nil)
	w := httptest.NewRecorder()
	r.ServeHTTP(w, req)

	assert.Equal(t, http.StatusBadRequest, w.Code)
}

func TestDeleteTaskNotFound(t *testing.T) {
	r := setupTestRouter()

	req, _ := http.NewRequest("DELETE", "/tasks/999", nil)
	w := httptest.NewRecorder()
	r.ServeHTTP(w, req)

	assert.Equal(t, http.StatusNotFound, w.Code)
}
