package main

import (
	"net/http"

	"fmt"
	"math/rand"
	"strconv"

	"github.com/gin-gonic/gin"
)

type Task struct {
	ID          int    `json:"id"`
	Title       string `json:"title"`
	Description string `json:"description"`
	Status      string `json:"status"`
}

var tasks = []Task{}

func setupRouter() *gin.Engine {
	r := gin.Default()

	r.GET("/tasks", GetTasks)
	r.GET("/tasks/:id", GetSingleTask)
	r.POST("/tasks", AddTask)
	r.PUT("/tasks/:id", UpdateTask)
	r.DELETE("/tasks/:id", DeleteTask)

	return r
}

func main() {
	// TODO Format error responses as JSON responses instead of strings
	// TODO Use some sort of golang doc to generate proper comments with return values for functions
	// TODO Use OpenAPI to develop API spec
	r := setupRouter()

	r.Run(":8080")
}

// Returns all Tasks
func GetTasks(c *gin.Context) {
	c.JSON(http.StatusOK, tasks)
}

// Returns the Task with the provided ID, if it exists
// Returns 200 with Task if successful
// Returns 400(BadRequest) if the ID could not be parsed correctly
// Returns 404 (NotFound) if the ID doesn't exist
func GetSingleTask(c *gin.Context) {
	stringId := c.Param("id")
	taskId, err := strconv.Atoi(stringId)

	if err != nil {
		c.JSON(http.StatusBadRequest, "Task ID could not be parsed.")
		return
	}

	for _, t := range tasks {
		if t.ID == taskId {
			c.JSON(http.StatusOK, t)
			return
		}
	}
	errorMessage := fmt.Sprintf("Task with ID %v was not found.", taskId)
	c.JSON(http.StatusNotFound, errorMessage)
}

// Creates a new Task, adds it to array of all tasks, and returns the new Task
// Returns 200 with new Task if successful
// Returns 400 (Bad Request) if the Task's POST data could not be parsed correctly or if the Task data was not valid
func AddTask(c *gin.Context) {
	var newTask Task
	err := c.BindJSON(&newTask)

	if err != nil {
		c.JSON(http.StatusBadRequest, "Error parsing JSON body data.")
		return
	}

	if !isValidTask(newTask) {
		c.JSON(http.StatusBadRequest, "Task data was not valid.")
		return
	}

	newTask.Status = "Pending"

	newId := rand.Int()
	// TODO Check if there is ID collision
	newTask.ID = newId

	tasks = append(tasks, newTask)
	c.JSON(http.StatusOK, newTask)
}

// Updates a task with values passed in payload
// Returns 200 with updated Task data if successful
// Returns 400 (Bad Request) if the Task's POST data could not be parsed correctly or if the Task data was not valid
// Returns 404 (Not Found) if the Task with the given ID could not be found// Returns
func UpdateTask(c *gin.Context) {
	stringId := c.Param("id")
	taskId, err := strconv.Atoi(stringId)

	if err != nil {
		c.JSON(http.StatusBadRequest, "Task ID could not be parsed.")
		return
	}

	var updatedTask Task
	err = c.BindJSON(&updatedTask)

	if err != nil {
		c.JSON(http.StatusBadRequest, "Error parsing JSON body data.")
		return
	}

	if updatedTask.ID != taskId {
		c.JSON(http.StatusBadRequest, "Task ID in the URL and body do not match.")
		return
	}

	if !isValidTask(updatedTask) {
		c.JSON(http.StatusBadRequest, "Task data was not valid.")
		return
	}

	for i, t := range tasks {
		if t.ID == updatedTask.ID {
			// TODO Probably a cleaner way to do this
			// Can maybe be done with slice.replace
			tasks[i].Description = updatedTask.Description
			tasks[i].Status = updatedTask.Status
			tasks[i].Title = updatedTask.Title

			c.JSON(http.StatusOK, tasks[i])
			return
		}
	}

	c.JSON(http.StatusNotFound, fmt.Sprintf("Task with ID %v was not found.", updatedTask.ID))
}

// Deletes a Task from the slice of all Tasks
// Returns 200 with Task data if successful
// Returns 400 (Bad Request) if the Task ID could not be parsed
// Returns 404 (Not Found) if the Task with the given ID could not be found
func DeleteTask(c *gin.Context) {
	stringId := c.Param("id")
	taskId, err := strconv.Atoi(stringId)

	if err != nil {
		c.JSON(http.StatusBadRequest, "Task ID could not be parsed.")
		return
	}

	for i, t := range tasks {
		if t.ID == taskId {
			tasks = append(tasks[:i], tasks[i+1:]...)

			c.Status(http.StatusNoContent)
			return
		}
	}
	errorMessage := fmt.Sprintf("Task with ID %v was not found.", taskId)
	c.JSON(http.StatusNotFound, errorMessage)
}

// Returns true IFF Task description and title are not empty, and status is
// one of either 'Pending', 'In Progress', or 'Completed', otherwise returns false
func isValidTask(task Task) bool {
	// Both description and title need to not be empty to be valid
	if task.Description == "" || task.Title == "" {
		return false
	}

	// If the status is not empty (eg. a task is being updated),
	// it needs to be either 'Pending', 'In Progress', or 'Completed'
	if task.Status != "" && task.Status != "Pending" && task.Status != "In Progress" && task.Status != "Completed" {
		// TODO Fix potential issues with capitalization or whitespace with statuses being passed failing this check
		return false
	}
	return true
}
